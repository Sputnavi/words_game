using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Words
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Title = "Words";
            List<string> words = new List<string>();
            words.Add(GetFirstWord());
            words.Add("");
            StartGame(words);
        }

        static void StartGame(List<string> words)
        {
            Console.Clear();
            Console.WriteLine("Главное слово - {0}\n", words[0]);

            int winnerNumber = SwitchPlayer(GetLoser(words));
            FinishGame(winnerNumber);
        }

        static int SwitchPlayer(int playerNumber) => playerNumber == 1 ? 2 : 1;

        static void FinishGame(int WinnerNumber)
        {
            Console.WriteLine("\nИгрок #{0} победил!", WinnerNumber);
        }

        static string GetFirstWord()
        {
            Console.Write("Введите главное слово: ");
            string word = Console.ReadLine();

            if (word.Length >= 8 && word.Length <= 30)
                return word;

            Console.WriteLine("Слово не подходит, введите новое.\n");
            return GetFirstWord();
        }

        // Method of game proccess, which returns a loser.
        static int GetLoser(List<string> words, int currentPlayerNumber = 1)
        {
            Console.Write($"Ход игрока #{currentPlayerNumber}: ");
            string playerWord = Console.ReadLine();

            bool badWord = !IsWordGood(words, playerWord);
            if (badWord)
                return currentPlayerNumber;

            words.Add(playerWord);
            return GetLoser(words, SwitchPlayer(currentPlayerNumber));
        }

        // Handles common cases.
        static bool IsWordGood(List<string> words, string playerWord)
        {
            if (words.Contains(playerWord))
            {
                Console.WriteLine("Слово уже встречалось.");
                return false;
            }

            if (words[0].Length < playerWord.Length)
            {
                Console.WriteLine("Введенное слово больше главного слова.");
                return false;
            }

            return CanBeBuilt(playerWord, words[0]);
        }

        // Checks if it possible to build a word using the main word.    
        static bool CanBeBuilt(string playerWord, string firstWord)
        {
            if (playerWord == "")
                return true;

            // The check is that we can delete all characters in
            // player word before they end in the main word.
            int index = firstWord.IndexOf(playerWord[0]);
            if (index == -1)
            {
                Console.WriteLine("Невозможно составить слово.");
                return false;
            }
            firstWord = firstWord.Remove(index, 1);
            playerWord = playerWord.Remove(0, 1);

            return CanBeBuilt(playerWord, firstWord);
        }
    }
}